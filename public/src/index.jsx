import React, { Component } from 'react';
import ReactDOM from "react-dom";
import {createStore, applyMiddleware} from 'redux';
import {Provider} from 'react-redux';
import thunk from 'redux-thunk';
import {createLogger} from 'redux-logger';
import promise from 'redux-promise';
import allReducers from './reducers';
import "./styles/main.scss";

import App from "./components/App";

const logger = createLogger({});

const store = createStore(
    allReducers,
    applyMiddleware(logger, promise, thunk)
)

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider> , document.getElementById("app"));
