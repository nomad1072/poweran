export function loginUser(user){
    return {
        type: "LOG_IN_USER",
        payload: user
    }
}

export function logoutUser(){
    return {
        type: "LOG_OUT_USER"
    }
}