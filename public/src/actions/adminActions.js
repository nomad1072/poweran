import fetch from 'cross-fetch';

import { addNotification } from './notificationsActions';

// Dev
const SERVER = "http://localhost:8000";

// Production
// const SERVER = "/";

//ONLY FOR ADMIN
export function loadAccounts(dispatch){
    return fetch(`${SERVER}/accounts`)
        .then(response => response.json())
        .then(function(data){
            return dispatch({
                type: "LOAD_ACCOUNTS",
                payload: data
            })
        }).catch(err => console.error("Error fetching accounts details: ", err))
}

export function editAccount(dispatch, account){
    return fetch(`${SERVER}/account/edit`, {
        method: "post",
        headers: {
            "content-type": "application/json"
        },
        body: JSON.stringify({
            id: account.id,
            enabled: !account.enabled
        })
    })
    .then(response => response.json())
    .then(data => {
        dispatch({
            type: "EDIT_ACCOUNT",
            payload: data
        })
        addNotification(dispatch, "Account updated")
    }).catch(err => addNotification(dispatch ,"Error editing account enable status"));
}

export function deleteAccount(dispatch, id){
    return fetch(`${SERVER}/account/delete`, {
        method: "post",
        headers: {
            "content-type": "application/json"
        },
        body: JSON.stringify({
            id: id
        })
    })
    .then(response => response.json())
    .then(data => {
        dispatch({
            type: "DELETE_ACCOUNT",
            payload: id
        });
        addNotification(dispatch, "Account deleted.");
    }).catch(err => addNotification( dispatch, "Error deleting account"));
}

export function updateAdminPassword(dispatch, obj){
    return fetch(`${SERVER}/admin/changepassword`, {
        method: "post",
        headers: {
            "content-type": "application/json"
        },
        body: JSON.stringify({
            pass_old: obj.pass_old,
            pass_new: obj.pass_new
        })
    })
    .then(response => response.json())
    .then(data => {
        if(data.success){
            addNotification(dispatch, "Password changed!");
        }else{
            addNotification( dispatch , data.message);
        }
    })
}