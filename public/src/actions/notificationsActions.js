export function addNotification(dispatch, notif){
    var id = new Date().getTime(); // assign unique id here
    dispatch({
        type: "ADD_NOTIFICATION",
        payload: {
            id: id,
            message: notif
        }
    });
    setTimeout(()=> {
        dispatch({
            type: "REMOVE_NOTIFICATION",
            payload: id
        })
    }, 3000);
    return id;
}

export function removeNotification(dispatch, id){
    dispatch({
        type: "REMOVE_NOTIFICATION",
        payload: id
    })
}

export function clearNotifications(dispatch){
    dispatch({
        type: "CLEAR_NOTIFICATIONS"
    })
}