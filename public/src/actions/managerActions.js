import fetch from 'cross-fetch'

import { addNotification } from './notificationsActions';

// Dev
const SERVER = "http://localhost:8000";

// Production
// const SERVER = "/";

export function getChildren(dispatch){
    return fetch(`${SERVER}/manager/getchildren`)
    .then(response => response.json())
    .then(data => {
        console.log(data);
        if(data.success){
            dispatch({
                type: "MANAGER_LOAD_USERS",
                payload: data.children
            })
        }else{
            addNotification(dispatch, `Could not load users list`);
        }
    })
    .catch(err => addNotification(dispatch, `Error while loading users: ${err}`));
}

export function editUserStatus(dispatch, user){
    return fetch(`${SERVER}/user/editStatus`, {
        method: "post",
        headers: {
            "content-type": "application/json"
        },
        body: JSON.stringify({
            id: user._id,
            enabled: !user.enabled
        })
    })
    .then(response => response.json())
    .then(data => {
        if(data.success){
            dispatch({
                type: "EDIT_USER_STATUS",
                payload: data.user
            })
            addNotification(dispatch, `User set to ${(data.user.enabled ? "Enabled" : "Disabled")}`)
        }else{
            addNotification(dispatch, `Could not change user status`);
        }
    }).catch(err => addNotification(dispatch, `Error editing user status: ${err}`));
}

export function deleteUser(dispatch, id){
    return fetch(`${SERVER}/user/delete`, {
        method: "post",
        headers: {
            "content-type": "application/json"
        },
        body: JSON.stringify({
            id: id
        })
    })
    .then(response => response.json())
    .then(data => {
        if(data.success){
            dispatch({
                type: "DELETE_USER",
                payload: id
            })
            addNotification(dispatch, `User deleted`);
        }else{
            addNotification(dispatch, "Could not delete user");
        }
    }).catch(err => addNotification(dispatch, `Error deleting USER: ${err}`));
}

export function createUser(dispatch, obj){
    return fetch(`${SERVER}/user/create`, {
        method: "post",
        headers: {
            "content-type": "application/json"
        },
        body: JSON.stringify({
            email: obj.email,
            fd: obj.fd
        })
        
    })
    .then(response => response.json())
    .then(data => {
        dispatch({
            type: "CREATE_USER",
            payload: data.user
        })
        addNotification(dispatch , `New User ${obj.email} added`);
    }).catch(err => console.error("Could not create user: ", err));
}

export function updateManagerPassword(dispatch, obj){
    return fetch(`${SERVER}/manager/changepassword`, {
        method: "post",
        headers: {
            "content-type": "application/json"
        },
        body: JSON.stringify(obj)
    })
    .then(response => response.json())
    .then(data => {
        if(data.success){
            addNotification(dispatch, "Password changed!");
        }else{
            addNotification(dispatch, "Something went wrong. Please try again.");
        }
    })
}

export function updateManagerProfile(dispatch, obj){
    return fetch(`${SERVER}/manager/updateprofile`, {
        method: "post",
        headers: {
            "content-type": "application/json"
        },
        body: JSON.stringify(obj)
    })
    .then(response => response.json())
    .then(data => {
        if(data.success){
            dispatch({
                type: "MANAGER_UPDATE",
                payload: obj
            })
            addNotification(dispatch, "Profile updated!");
        }else{
            console.log(data);
            addNotification(dispatch, "Something went wrong. Please try again.");
        }
    })
}

export function loadManagerFeatures(dispatch, productName){
    
    return fetch(`${SERVER}/manager/features/load/${productName}`)
        .then(response => response.json())
        .then(function(data){
            console.log(data);
            dispatch({
                type: "LOAD_MANAGER_FEATURES",
                payload: data
            })
        }).catch(err => console.error("Could not load manager's products: ", err));
}

export function handleFeatureUpdate(dispatch, obj){
    var featureLabelToDisplay = {
        realtime: "Realtime Update Hooks",
        daily: "Daily Dump",
        period: "Periodic Data"
    }
    return fetch(`${SERVER}/manager/features/update/${obj.name}`, {
        method: "post",
        headers: {
            "content-type": "application/json"
        },
        body: JSON.stringify(obj.values)
    })
    .then(response => response.json())
    .then(function(data){
        if(data.success){
            addNotification(dispatch, `Settings updated for feature: ${featureLabelToDisplay[obj.name]}`);
            dispatch({
                type: "UPDATE_MANAGER_FEATURE",
                payload: obj
            })
        }else{
            addNotification(dispatch, `Could not update settings for features: ${featureLabelToDisplay[obj.name]}`);
        }
        
    }).catch(err => addNotification(dispatch, `Could not update feature: ${featureLabelToDisplay[obj.name]}. ${err}`));
}