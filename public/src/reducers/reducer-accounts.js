export default function(state=null, action){
    switch(action.type){
        case "LOAD_ACCOUNTS":
            state = [
                ...action.payload
            ]
            break;
        case "EDIT_ACCOUNT":
            var myIndex = state.findIndex(elem => elem.id == action.payload.id);
            var tempState = [...state];
            tempState.splice(myIndex, 1, action.payload);
            return tempState;
            break;
        case "DELETE_ACCOUNT":
            var myIndex = state.findIndex(elem => elem.id == action.payload);
            var tempState = [...state];
            tempState.splice(myIndex, 1);
            return tempState;
            break;
        default:
            break;
    }
    return state;
}