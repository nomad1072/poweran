export default function(state=[], action){
    switch(action.type){
        case "ADD_NOTIFICATION":
        // payload is notification object with keys: id, message
        var tempState = state;
        tempState.push(action.payload);
        state = [...tempState];
        break;

        case "REMOVE_NOTIFICATION":
        // payload is notification id
        var myIndex = state.findIndex(notif => notif.id == action.payload);
        var tempState = state;
        tempState.splice(myIndex, 1);
        state = [ ...tempState ];
        break;

        case "CLEAR_NOTIFICATIONS":
        state = [];
        break;

        default:
        break;
    }
    return state;
}