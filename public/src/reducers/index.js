import {combineReducers} from 'redux';

import userReducer from './reducer-user';
import accountsReducer from './reducer-accounts';
import managerReducer from './reducer-manager';
import notificationsReducer from './reducer-notifications';

export default combineReducers({
    user: userReducer,
    accounts: accountsReducer,
    manager: managerReducer,
    notifications: notificationsReducer
})