export default function(state=null, action){
    switch(action.type){
        case "LOG_IN_USER":
        state = {
            ...action.payload
        }
        break;

        case "CREATE_USER":
        if(state.access_level == 1){
            var tempState = state;
            tempState.users.push(action.payload._id);
            state = {
                ...state,
                users: tempState.users
            }
        }
        break;

        case "MANAGER_UPDATE":
        if(state.access_level == 1){
            var tempState = state;
            tempState.users.push(action.payload._id);
            for(var key in action.payload){
                tempState[key] = action.payload[key];
            }
            state = {
                ...tempState
            }
        }
        break;

        case "LOG_OUT_USER":
        state = null;
        break;

        default:
            break;
    }
    return state;
}