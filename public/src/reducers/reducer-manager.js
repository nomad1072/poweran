export default function(state={}, action){
    switch(action.type){
        case "MANAGER_LOAD_USERS":
        state = {
            ...state,
            children: [...action.payload]
        }
        break;

        case "EDIT_USER_STATUS":
        var tempState = state;
        var myIndex = tempState.children.findIndex(user => user._id == action.payload._id);
        tempState.children.splice(myIndex, 1, action.payload);
        state = {
            ...state,
            children: [...tempState.children]
        }
        break;

        case "CREATE_USER":
        var tempState = state;
        tempState.children.push(action.payload);
        state = {
            ...state,
            children: [...tempState.children]
        }
        break;

        case "DELETE_USER":
        var tempState = state;
        var myIndex = state.children.findIndex(elem => elem._id == action.payload);
        
        tempState.children.splice(myIndex, 1);
        state = {
            ...state,
            children: [...tempState.children]
        }
        break;
        case "LOAD_MANAGER_FEATURES":
        console.log(action.payload);
        var tempState = state;
        if(!tempState.hasOwnProperty("features")){
            tempState.features = {};
        }
        for(var feature in action.payload.features){
            console.log(feature);
            tempState.features[feature] = action.payload.features[feature]
        }
        state = {
            ...state,
            features: {
                ...tempState.features
            }
        }
        break;

        case "UPDATE_MANAGER_FEATURE":
        console.log(action);
        if(state.features){
            var featuresBase = {
                ...state.features
            }
        }else{
            var featuresBase = {};
        }
        switch(action.payload.name){
            case "realtime":
            state = {
                ...state,
                features: {
                    ...featuresBase,
                    realtime: action.payload.values
                }
            }
            break;
    
            case "daily":
            state = {
                ...state,
                features: {
                    ...featuresBase,
                    daily: action.payload.values
                }
            }
            break;
    
            case "period":
            state = {
                ...state,
                features: {
                    ...featuresBase,
                    period: action.payload.values
                }
            }
            break;
    
            default:
            break;
        }
        break;

        default:
        break;
    }
    return state;
}