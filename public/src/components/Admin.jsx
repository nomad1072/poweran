import React, { Component } from 'react';
import {connect} from 'react-redux';
import {Route, Redirect} from 'react-router-dom';

import AdminAccountActions from './AdminAccountActions';
import AdminProfile from './AdminProfile';

class Admin extends Component {
    constructor(props){
        super(props);
        this.state = {
            windowId: 0
        }
        this.changeWindow = this.changeWindow.bind(this);
    }
    changeWindow(new_window_id){
        return () => {
            var tempState = this.state;
            tempState.windowId = new_window_id;
            this.setState(tempState);
        }
        
    }
    render(){
        var window = (null);
        switch(this.state.windowId){
            case 0:
                window = (<AdminAccountActions />)
                break;
            case 1:
                window = (<AdminProfile />)
                break;
            default:
                window = (<AdminAccountActions />)
                break;
        }
        if(this.props.user){
            switch(this.props.user.access_level){
                case 1:
                    return (<Redirect to={{ pathname: "/manager", from:this.props.location }} />)
                case 2:
                    return (<Redirect to={{ pathname: "/user", from:this.props.location }} />)
                default:
                    break;
            }
        }else{
            return (
                <Redirect to={{ pathname: "/", from:this.props.location }} />
            )
        }
        return (
            <div>
                <div className="buttons" >
                    <button className={ ["btn", (this.state.windowId == 0 ? "active" : "")].join(" ") } onClick={this.changeWindow(0)} >Accounts</button>
                    <button className={ ["btn", (this.state.windowId == 1 ? "active" : "")].join(" ") } onClick={this.changeWindow(1)} >Profile</button>
                </div>
                {
                    window
                }
            </div>
            
        )
    }
}

function mapStateToProps(state){
    return {
        user: state.user
    }
}

function mapDispatchToProps(dispatch){
    return {
        loginUser: (o) => dispatch(loginUser(o))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Admin);