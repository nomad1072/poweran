import React, { Component } from 'react';
import {connect} from 'react-redux'

import { updateAdminPassword } from '../actions/adminActions';
import { addNotification } from '../actions/notificationsActions';

class AdminProfile extends Component{
    constructor(props){
        super(props);
        this.state = {
            updateMessage: ""
        }
        
        this.handleChangePassword = this.handleChangePassword.bind(this);
    }

    handleChangePassword(e){
        e.preventDefault();
        
        if(this.refs.pass_new_1.value != this.refs.pass_new_2.value){
            this.props.addNotification("Passwords dont't match. Try again.");
        }else{
            this.props.updateAdminPassword({
                pass_old: this.refs.pass_old.value,
                pass_new: this.refs.pass_new_1.value
            })
        }
    }

    render(){
        return (
            // <div>
            //     <h1>
            //         Update Profile
            //     </h1>
                
            //     <h2>
            //         Change Password
            //     </h2>
            //     <form onSubmit={this.handleChangePassword} >
            //         <input placeholder="Old Password" type="password" name="old_password" ref="pass_old" /><br/>
            //         <input placeholder="New Password" type="password" name="old_password" ref="pass_new_1" /><br/>
            //         <input placeholder="Re-enter New Password" type="password" name="old_password" ref="pass_new_2" /><br/>
            //         <button type="submit">Change Password</button>
            //     </form>
            // </div>
            <div className="container">
                <div className="well clearfix tabhead">
                    <fieldset className="scheduler-border">
                        <legend className="scheduler-border">Update Profile</legend>
                        <form  onSubmit={this.handleChangePassword} className="form">
                        <div className="row">
                            <div className="form-group">
                            <div className="col-md-12">     
                                <label >Current Password</label>
                                <input  className="form-control"  placeholder="Old Password" type="password" name="old_password" ref="pass_old" required />
                            </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="form-group">
                            <div className="col-md-12">
                                <label >New Password</label>
                                <input className="form-control" placeholder="New Password" type="password" name="old_password" ref="pass_new_1" required />
                            </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-md-12">
                            <div className="form-group">
                            <label >Confirm Password</label>
                            <input  className="form-control" placeholder="Re-enter New Password" type="password" name="old_password" ref="pass_new_2" required />
                            </div>
                            </div>
                        </div>
                        <button  className="btn btn-primary" type="submit">Change Password</button>
                        </form>
                    </fieldset>
                </div>
            
            </div>
        )
    }
}

function mapStateToProps(state){
    return {
        user: state.user
    }
}

function mapDispatchToProps(dispatch){
    return {
        updateAdminPassword: (o) => updateAdminPassword(dispatch, o),
        addNotification: (o) => addNotification(dispatch, o)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AdminProfile);