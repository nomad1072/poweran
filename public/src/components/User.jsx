import React, { Component } from 'react';
import {connect} from 'react-redux';
import {Route, Redirect} from 'react-router-dom';

class User extends Component {
    constructor(props){
        super(props);
    }
    render(){
        if(this.props.user){
            switch(this.props.user.access_level){
                case 0:
                    return (<Redirect to={{ pathname: "/admin", from:this.props.location }} />)
                case 1:
                    return (<Redirect to={{ pathname: "/manager", from:this.props.location }} />)
                default:
                    break;
            }
        }else{
            return (
                <Redirect to={{ pathname: "/", from:this.props.location }} />
            )
        }
        return (
            <h1>
                User Dashboard
            </h1>
        )
    }
}
function mapStateToProps(state){
    return {
        user: state.user
    }
}

function mapDispatchToProps(dispatch){
    return {
        loginUser: (o) => dispatch(loginUser(o))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(User);