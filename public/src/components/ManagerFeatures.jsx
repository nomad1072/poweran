import React, { Component } from 'react';
import {connect} from 'react-redux';

import ManagerFeatureRealtime from './ManagerFeatureRealtime';
import ManagerFeatureDaily from './ManagerFeatureDaily';
import ManagerFeaturePeriod from './ManagerFeaturePeriod';

class ManagerFeatures extends Component {
    constructor(props){
        super(props);
        this.state = {
            windowId: 0
        }
    }
    changeWindow(new_window_id){
        return () => {
            var tempState = this.state;
            tempState.windowId = new_window_id;
            this.setState(tempState);
        }
    }
    render(){
        if(!this.props.user){
            return (null);
        }
        var window = (null);
        switch(this.state.windowId){
            case 0:
                window = (<ManagerFeatureRealtime />)
                break;
            case 1:
                window = (<ManagerFeatureDaily />)
                break;
            default:
                window = (<ManagerFeaturePeriod />)
                break;
        }
        return (
            <div className="managerFeatures">
                <div className="managerFeatureContainer">
                    <div className="sidebar" >
                        <button className={["btn" , (this.state.windowId == 0 ? "btn-warning" : "btn-primary")].join(" ")} onClick={this.changeWindow(0)} >Realtime</button>
                        <button className={["btn" , (this.state.windowId == 1 ? "btn-warning" : "btn-primary")].join(" ")} onClick={this.changeWindow(1)} >Daily Dump</button>
                        <button className={["btn" , (this.state.windowId == 2 ? "btn-warning" : "btn-primary")].join(" ")} onClick={this.changeWindow(2)} >Period Data</button>

                    </div>
                    <div className="article" >
                        {
                            window
                        }
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state){
    return {
        user: state.user
    }
}

function mapDispatchToProps(dispatch){
    return {
        loginUser: (o) => dispatch(loginUser(o))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ManagerFeatures);