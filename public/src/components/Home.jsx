import React, { Component } from 'react';
import {connect} from 'react-redux';
import { Redirect } from "react-router-dom";
import Login from './Login';
import Signup from './Signup';
import logo from '../../img/logo.jpg';

class Home extends Component {
    constructor(props){
        super(props);
        

    }
    render(){
        if(this.props.user){
            switch(this.props.user.access_level){
                case 0:
                    return (<Redirect to={{ pathname: "/admin", from:this.props.location }} />)
                case 1:
                    return (<Redirect to={{ pathname: "/manager", from:this.props.location }} />)
                case 2:
                    return (<Redirect to={{ pathname: "/user", from:this.props.location }} />)
                default:
                    break;
            }
        }
        return (
            <div id="home">
                
                
                <div className="container">
                    {/* <div className="page-header">
                        <h1><img src={logo} height="60px" /><span className="pull-right label label-default"></span></h1>
                    </div> */}
                    <div className="row">
                        <div className="col-md-6"></div>
                        <div className="col-md-6">
                            <div className="panel with-nav-tabs panel-default">
                                <div className="panel-heading">
                                        <ul className="nav nav-tabs">
                                            <li className="active"><a href="#login" data-toggle="tab" style={{"textAlign":"center", "width":"255px"}}><h4>LOGIN</h4></a></li>
                                            &nbsp;&nbsp;&nbsp;<li><a href="#signup" data-toggle="tab" style={{"textAlign":"center", "width":"250px"}}><h4>SIGN UP</h4></a></li>
                                        </ul>
                                </div>
                                <div className="panel-body">
                                    <div className="tab-content">
                                        <Login/>
                                        <Signup />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

function mapStateToProps(state){
    return {
        user: state.user
    }
}

function mapDispatchToProps(dispatch){
    return {
        loginUser: (o) => dispatch(loginUser(o))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);