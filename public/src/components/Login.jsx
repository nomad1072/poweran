import React, { Component } from 'react';
import fetch from 'cross-fetch';
import {connect} from 'react-redux';
import {Route, Redirect} from 'react-router-dom';

import { addNotification } from '../actions/notificationsActions';
import { loginUser } from '../actions/currentUserActions';

class Login extends Component {
    constructor(props){
        super(props)

        this.handleLogin = this.handleLogin.bind(this);
    }
    handleLogin(e){
        e.preventDefault();
        var self = this;
        fetch("http://localhost:8000/login", {
            method: "post",
            headers: {
                "content-type": "application/json"
            },
            body: JSON.stringify({
                email: this.refs.email.value,
                password: this.refs.password.value
            })
        })
        .then(response => response.json())
        .then(function(data){
            if(data.loggedIn){
                self.props.loginUser(data.user);
            }else{
                self.props.addNotification("Could not Sign in");
            }
            
        })
        .catch(err => console.error(err));

        fetch
    }
    render(){
        if(this.props.user){
            switch(this.props.user.access_level){
                case 0:
                    return (<Redirect to={{ pathname: "/admin", from:this.props.location }} />)
                case 1:
                    return (<Redirect to={{ pathname: "/manager", from:this.props.location }} />)
                case 2:
                    return (<Redirect to={{ pathname: "/user", from:this.props.location }} />)
                default:
                    return (<Redirect to={{ pathname: "/user", from:this.props.location }} />)
            }
        }else{
            return (
                // <div id="login" onSubmit={this.handleLogin}>
                //     <form>
                //         <span className="item"><input className="form-control" defaultValue="a" type="text" name="email" ref="email" placeholder="Email" /><br/>
                //         </span>
                //         <span className="item"><input className="form-control" defaultValue="a" type="password" name="password" ref="password" placeholder="Password" /><br/>
                //         </span>
                //         <span className="item"><button className="btn btn-primary" type="submit" >Sign In</button></span>
                        
                //     </form>
                    
                // </div>
                <div  onSubmit={this.handleLogin} className="tab-pane fade in active" id="login">
                    <form>
                        <div className="form-group">
                            <label>Email address:</label>
                            <input className="form-control f1" type="email" name="email" ref="email" placeholder="Email" required />
                        </div>
                        <div className="form-group">
                            <label>Password:</label>
                            <input className="form-control" type="password" name="password" ref="password" placeholder="Password" required/>
                        </div>
                        <button className="btn btn-primary btn-block" type="submit" >Sign In</button>
                    </form>
                </div>
            )
        }
        return null;
        
    }
}

function mapStateToProps(state){
    return {
        user: state.user
    }
}

function mapDispatchToProps(dispatch){
    return {
        loginUser: (o) => dispatch(loginUser(o)),
        addNotification: (msg) => addNotification(dispatch, msg)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);