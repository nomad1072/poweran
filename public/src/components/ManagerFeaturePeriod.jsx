import React, { Component } from 'react';

import {connect} from 'react-redux';

import { loadManagerFeatures, handleFeatureUpdate } from '../actions/managerActions';

class ManagerFeaturePeriod extends Component {
    constructor(props){
        super(props);

        this.handleUpdate = this.handleUpdate.bind(this);
    }

    componentWillMount(){
        this.props.loadManagerFeatures("period");
    }

    handleUpdate(e){
        e.preventDefault();
        var obj = {
            name: "period",
            values: {}
        };
        if(!this.refs.from_date.value){
            alert(`"From date" is a required field.`);
            return;
        }
        var entities = [];

        for (var ref in this.refs){
            if(ref.startsWith("get_")){
                entities.push(ref);
            }
        }
        for (var entity in entities){
            obj.values[entities[entity]] = this.refs[entities[entity]].checked
        }
        obj.values["from_date"] = this.refs.from_date.value;
        
        // console.log(obj.values);
        this.props.handleFeatureUpdate(obj);

    }

    render(){
        return (
            <div>
                
                {
                    this.props.manager && this.props.manager.features && this.props.manager.features.period ?
                    (
                        <div className="well clearfix tabhead">
                            <fieldset className="scheduler-border">
                                <form  onSubmit={this.handleUpdate} className="form">
                                    {
                                        Object.keys(this.props.manager.features.period).filter(elem => elem.startsWith("get_")).map(key => 
                                            (
                                                <span style={{ display: "inline-block", margin: "10px" }} key={key} >
                                                    <input style={{ margin: "0 4px" }} defaultChecked={this.props.manager.features.period[key]} type="checkbox" ref={key} />
                                                    <span><strong>{key.slice(4).split("_").join(" ")}</strong></span>
                                                    <br/>
                                                </span>
                                                
                                            )
                                        )
                                    }
                                    <br/>
                                    <span>
                                        <strong>From</strong><input className="form-control" ref="from_date" type="date"/>
                                    </span>
                                    <br/>
                                    <button className="btn btn-primary" type="submit">Set</button>
                                </form>
                            </fieldset>
                        </div>
                        
                    ) :
                    (
                        // <div>
                        //     <form onSubmit={this.handleUpdate} >
                        //         <input type="checkbox" ref="get_tickets" /><span><strong>Tickets</strong></span>
                        //         <br/>
                        //         <input type="checkbox" ref="get_contacts" /><span><strong>Contacts</strong></span>
                        //         <br/>
                        //         <input type="checkbox" ref="get_companies" /><span><strong>Companies</strong></span>
                        //         <br/>
                        //         <span><strong>From:</strong><input ref="from_date" type="date"/></span>
                        //         <br/>
                        //         <button type="submit">Set</button>
                        //     </form>
                        // </div>
                        <div className="well clearfix tabhead">
                            <fieldset className="scheduler-border">
                                <form  onSubmit={this.handleUpdate} className="form">
                                    <div style={{ margin: "20px auto" }} className="row">
                                        <div className="form-group">
                                            <div className="col-md-12">
                                                <input style={{ margin: "0 10px" }} type="checkbox" ref="get_tickets" />
                                                <span><strong>Tickets</strong></span>

                                                <input style={{ margin: "0 10px" }} type="checkbox" ref="get_contacts" />
                                                <span><strong>Contacts</strong></span>

                                                <input style={{ margin: "0 10px" }} type="checkbox" ref="get_companies" />
                                                <span><strong>Companies</strong></span>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div style={{ margin: "20px auto" }} className="row">
                                        <div className="form-group">
                                            <div className="col-md-12">
                                            <strong>From</strong><input className="form-control" ref="from_date" type="date"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div style={{ margin: "20px auto" }} className="row">
                                        <div className="form-group">
                                            <div className="col-md-12">
                                                <button className="btn btn-primary" type="submit">Set</button>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </form>
                            </fieldset>
                        </div>
                    )
                }
            </div>
        )
    }
}

function mapStateToProps(state){
    return {
        user: state.user,
        manager: state.manager
    }
}

function mapDispatchToProps(dispatch){
    return {
        loadManagerFeatures: (productName) => loadManagerFeatures(dispatch, productName),
        handleFeatureUpdate: (o) => handleFeatureUpdate(dispatch, o)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ManagerFeaturePeriod);