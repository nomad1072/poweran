import React, { Component } from 'react';
import {connect} from 'react-redux';

import {getChildren} from '../actions/managerActions';
import ManagerTableRow from './ManagerTableRow';
import ManagerCreateUserRow from './ManagerCreateUserRow';

class ManagerAccountActions extends Component{
    constructor(props){
        super(props);
    }
    componentWillMount(){
        this.props.getChildren();
    }
    render(){
        return (
            <div>
                {
                    this.props.children ?
                    (
                        <div className="container">
                        <div className="col-md-1"></div>
                          <div className="col-md-10">
                          <ManagerCreateUserRow />
                        <table className="table table-hover">
                          <thead>
                            <tr>
                              <th>Email</th>
                              <th>Enabled/Disabled</th>
                              <th style={{width: "100px"}} >Actions</th>
                            </tr>
                          </thead>
                          <tbody style={{textAlign:"center"}}>
                          {
                                this.props.children.map(function (child){
                                    return (
                                        <ManagerTableRow key={child._id} child={child} />
                                    )
                                })
                            }
                          </tbody>
                        </table>
                        </div>
                      </div>
                    ) :
                    (
                        <span>Loading account details...</span>
                    )
                }
            </div>
            
        )
    }
}

function mapStateToProps(state){
    return {
        user: state.user,
        children: state.manager.children
    }
}

function mapDispatchToProps(dispatch){
    return {
        getChildren: () => getChildren(dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ManagerAccountActions);