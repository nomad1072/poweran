import React, { Component } from 'react';
import {connect} from 'react-redux';
import {Route, Redirect} from 'react-router-dom';
import fetch from 'cross-fetch';

import {logoutUser} from '../actions/currentUserActions';

class Logout extends Component {
    constructor(props){
        super(props);
    }
    render(){
        var self = this;
        if(!this.props.user){
            return (
                <Redirect to={{ pathname: "/", from:this.props.location }} />
            )
        }else{
            fetch("http://localhost:8000/logout")
            .then(response => response.json())
            .then(function(data){
                self.props.logoutUser();
            })
            .catch(err => console.error("error while logging out: ", err));
        }
        return (
            <p>
                Logging you out...
            </p>
        )
    }
}
function mapStateToProps(state){
    return {
        user: state.user
    }
}

function mapDispatchToProps(dispatch){
    return {
        logoutUser: () => dispatch(logoutUser())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Logout);