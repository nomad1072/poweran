import React, { Component } from 'react';
import {connect} from 'react-redux';

import {editUserStatus, deleteUser} from '../actions/managerActions';

class ManagerTableRow extends Component {
    constructor(props){
        super(props)
        this.handleDelete = this.handleDelete.bind(this);
    }

    handleDelete(){
        var ask = confirm(`Are you sure you want to delete user ${this.props.child.email}?`);

        if(ask){
            this.props.deleteUser(this.props.child._id)
        }
    }

    render(){
        return (
            <tr key={this.props.child._id} >
                <td>{this.props.child.email}</td>
                <td>{ this.props.child.enabled ? "Enabled" : "Disabled" }</td>
                <td>
                    <button className="btn btn-default" onClick={() => this.props.editUserStatus(this.props.child)} >{!this.props.child.enabled ? "Enable": "Disable"}</button>
                    <button className="btn btn-danger" onClick={this.handleDelete}>Delete</button>
                </td>
            </tr>
        )
    }
}

function mapStateToProps(state){
    return {
        
    }
}

function mapDispatchToProps(dispatch){
    return {
        editUserStatus: (user) => editUserStatus(dispatch, user),
        deleteUser: (id) => deleteUser(dispatch, id)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ManagerTableRow);