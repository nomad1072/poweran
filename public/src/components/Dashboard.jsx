import React, { Component } from 'react';
import {connect} from 'react-redux';
import {Route, Redirect} from 'react-router-dom';
import PowerbiEmbedded from 'react-powerbi';

class Dashboard extends Component {
    constructor(props){
        super(props);

        // this.state = {
        //     windowId: 0
        // }
        // this.changeWindow = this.changeWindow.bind(this);
    }
    changeWindow(new_window_id){
        return () => {
            var tempState = this.state;
            tempState.windowId = new_window_id;
            this.setState(tempState);
        }
        
    }
    render(){
        
        // if(this.props.user){
        //     switch(this.props.user.access_level){
        //         case 0:
        //             return (<Redirect to={{ pathname: "/admin", from:this.props.location }} />)
        //         case 2:
        //             return (<Redirect to={{ pathname: "/user", from:this.props.location }} />)
        //         default:
        //             break;
        //     }
        // }else{
        //     return (
        //         <Redirect to={{ pathname: "/", from:this.props.location }} />
        //     )
        // }
        return (
            <div class="container-report">
                <PowerbiEmbedded
                    id={`f4a86b9f-4355-43a2-8244-c3b6e877ac79`}
                    embedUrl={`https://app.powerbi.com/view?r=eyJrIjoiNTg1NDE5NWQtNzJlOC00MGIzLTg2ZDQtNjI1MTIyMWZiZTM0IiwidCI6ImY3NDI4Y2YzLWQ4MGQtNDIyMS1hOTY5LWExMzliZGU1OTFkMSJ9`}
                    accessToken={`H4sIAAAAAAAEAB1WxwrsCBL7l3f1gnNamINzztk3Z3c75zDsv2_PHAtEIaQSpb__2NnTT1n5579_5NQ5enlWAH0p-ReOS72H-cTekZAtGPSDAHiyVfk3YDQzAPXXaCMVhV4OjJ37KQoi0Y3plnX4KymfKnp63BFlLQnuTgcPv364IkC44tYErQOenL-abgetMmZHdymsxmOAUGxN3YVmr0jJWzEWgHXP8b2kJUowYrBLSOWFwGbq74-Sh-vBkeEAXqYpydZvjWFUZWiDQQ3fE90WRiwuO6-4IWZTyT--7WUJQ3lsR-j5dr0dSyZ9zr4Qw0NpZP1-2Gtk-6U8eAH3r55uQIRW71SzelATQBM2L-gohmP_mPGtNOOunXIQXYZQ2I3LhdiueJb0ARi_UFomrPkr_9D9AbKiYu5GgE8kQ5lkFJOsnU9n0NS79eRnZQOtn4IKfYEed7ymCIEVQnriPJ6AqB1MdhCx9g0GRJFWPIoSOswVH9vjUluxSrOizM0weKAPo1vGzEswWwBaGgtnkD0BPQRnMJ-yztVij7q_ADpGHHCoDBxJ2hdSYzfhpyXFgIf2jvV60H2nMLhNh8hakAYFgXgD09sOlfJgUTX5cIgMLUnZCJ739IfToiA9g-5sYpLH82arkkQoUFjHvNN3cRSjJQlHnz4_m_c2J9AM6h9NK6PdOEDMrsQG7hbfu912wYL0ZlAaGSwlCR6hsXmWnNnChWriEVaX8WL9vDpFjGknXrft2ehObjr-fHf4TB3JC41aHg_61TRqfulAdzcTSrJ-fBi4Mz2G8dASapOxzlLSBAeivSG_5GUSKXPoQTzNgnW_mtJm4kIZJBQougSIT9Qh-p3QwJS-i51PVIEPSE5arrIanH66b5YZeY9hH0UivB3eA-HQXzsm6T0W-QbiLJBEAb4aNBq7VJ-bFspk1tULCRUgI_x-wtNgQ8WBh6e3JxbbiABidsd5F3nFik3vfW5T4Wb4xLiHzWgv3JnA9p5OeptsMebutI7mttxl-84NhvhwnDnoBb2OCluCby8w8ApDv73D6c3X2YscWhYePSG3WBrk4aEPRgOFN6iT-8r7Rr3wqVE5RFjZqrUB7zRNbsovAFCseYOkl6jmhrE7x_QC9c7X4kLfRjZdjsEGhkCuHOFsnj8k1NR7jwopfWi3s7CkIv_F2RndyOV240uyd6rizyFUYYMOwoTVNJgDUyybKTkd9xDandXiRR_C2bdmsG5JbBMoZcUnt1W_lA52QGFiuNBhpYZV1OoFpxAFkVuJ7RfLOaL0GX2GPHjIlYZmT2bKZ3zAXrwlRbV_Fafqa7HBtnh11LpZAHneHEwkkPptlemB94oQKJJhqVi9B2Q_8QOkMjD8aIjQopQirkqOStPnDrw4OfdfilNtjlt4ETrWJgmupTmqLwaqIyGD9OZIg8StHIaw8yuTnx5qK_S5j8UXJaoWcx2tWq14JS8rrJm9yROLZ-mgfYq5aqHkIqXvo91wiwLlMwYzAlDv6iVsH4VTCfEShH1qc8uO-I236G5wcHiMVtCNWxC_pY68SkyMMHWCXynvARDovruuKnNtJXqTKJvyDT5kDMuJoqkROCXB0uJLdxq4Xyv98Ajyfptbrx6O4J_dGPYwgeMszSIOLLxrTN6nEHbs3g2Rbl_N3X3QQJSY1XCfd43o5KPL36qaWeQyz5VD7Uu3XOTs_AIS9HRudsxdZokrugOW8DeEIMn-evQu2nU9ClLoUe8WWR9XfiwJZLmbTwktdQHgGT6628Vjg0TMiSf6LerAXfrOttmIpgh99hueoSPwbmiPg-P721fO-HgTK8dldP86FNAtoL1ZE7HGDz8hcgbNvwMh4m2I47GLu3ABiafiMO_igAhGFgVzJJ4h2Mh3HQKDl2bvHGO3RkP2MD9uShacgYsxXbC0_aCFZMW1Dfo1Mmz4RV6Vo3eiTIdMtyeKqkb7SmjHFWkgC7XHNAD21MI3c0yJNaRuwsJkGgz1Z82y9Wbi3gLYbJojQg3sNWUlrAOgjog8TPyRdZXerpXBaunTsDtrRvzIV9o1S7f_kXp604gbsUNVRgYUzKAjaj9A8llQ7oWIWg-cd99TXju331fRMYssZQtpo1SeG52f1rY32WthAUpTDJCSTqDnJPMYRqaJ9IQWuKTWVS9vMjb6xjUWaa25LzQAknmNemSMqaEneFpg5cj15z9_uPWZ90mrnl-dWDhCsVev_0Z38kazqWqgLmwluqLFZhbkuqZrP_ZwxGZXLu95Z5IvipufB5mlEBDiaaxNXQZ6Nfq5LrS8tTXRghcunI6khhpxoXl-w3pzwOWRxbjOFEEZ9U23KfsKovHe3nCWgvzbwb5nGnhgoBnHSfXYSdlK7H29rry5gbYqEsmYN2JDmvLn2m8_kdwKKFR1TijkwCYqDLlMUH2IsNXHnJAahgl6BTBERmCH3Pl5RtEPUl_LdWUqeZ73lBZdxELtGABVMfqZGz761Z7wW611CuooTpwt_hFgYHPdXLT0ggIE2tBHdbbhMDrXa34os10_8M7Ip8b67d57xaPxJukVh5Ref_31j8zP3FarEv5Utv2O0ysUyaAaqNwq_PGLOOZflPdpxmw_1uoHc3N0DdtbKiVtBCfK-RKK5QVr2rUBVPawmIlbwzcEiB6S9Vj49eKpWk8NhOBqVxDmuL48erALgO3Wyld9xI1ZsFwCaydCbOwzhGeIWJ8ihj83Ce8K17PDJB78h87NvvlWZal1cIYjtU5CF-N_0MyiN9QBwNd26jOu9CBRdL5bG4Dfp2MKXyyOZLKpixloeksO27TXXZM6SQio1AasnFMjrRNaiBFYIFILbrZMjSAcCw1sv9G5Kb3Xa7-KZXhY4HMjyrZ82Ky6rlY1zjwMuGQbWBQSNZ5Qix8u_cqG6UFimMihQHY0oMxL8oGXMSgANm6IWdwX977yTchkeDsrpb_4l6ubn8z_-z_Tm2WIQgsAAA==`}
                    filterPaneEnabled={false}
                    navContentPaneEnabled={false}
                    // pageName={`ReportSectioneb8c865100f8508cc533`}
                    // embedType={`EMBED`}
                    width='600px'
                    height='900px'
                    />
            </div>
        )
    }
}
function mapStateToProps(state){
    return {
        user: state.user
    }
}

function mapDispatchToProps(dispatch){
    return {
        // loginUser: (o) => dispatch(loginUser(o))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);