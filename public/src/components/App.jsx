import React, { Component } from 'react';
import { Redirect, BrowserRouter as Router, Route, Link } from "react-router-dom";
import fetch from 'cross-fetch';
import {connect} from 'react-redux';

import Home from './Home';
import Admin from './Admin';
import Manager from './Manager';
import User from './User';
import Logout from './Logout';
import Nav from './Nav'
import Notifications from './Notifications';

import {loginUser} from '../actions/currentUserActions';

class App extends Component {
    componentWillMount(){
        var self = this;
        fetch("http://localhost:8000/currentUser")
        .then(response => response.json())
        .then(function(data){
            if(data.user){
                self.props.loginUser(data.user);
            }
        }).catch(err => console.error("error initialising user: ", err))
    }
    render(){
        return (
            <div id="root">
                <Router >
                    <div>
                        <Nav />
                        <Notifications />

                        <Route exact path="/" component={Home} />
                        <Route path="/admin" component={Admin} />
                        <Route path="/manager" component={Manager} />
                        <Route path="/user" component={User} />
                        <Route path="/logout" component={Logout} />
                    </div>
                    
                </Router>
            </div>
        )
    }
}

function mapStateToProps(state){
    return {
        user: state.user
    }
}

function mapDispatchToProps(dispatch){
    return {
        loginUser: (o) => dispatch(loginUser(o))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);