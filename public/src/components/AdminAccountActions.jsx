import React, { Component } from 'react';
import {connect} from 'react-redux';

import {loadAccounts} from '../actions/adminActions';

import AccountsTableRow from './AccountsTableRow';

class AdminAccountActions extends Component {
    componentWillMount(){
        this.props.loadAccounts();
    }
    render(){
        return (
            <div>
                {
                    this.props.accounts ?
                    (
                        <div className="container">
                        <div className="col-md-1"></div>
                          <div className="col-md-10">
                        <table className="table table-hover">
                          <thead>
                            <tr>
                              <th>Email</th>
                              <th>Domain</th>
                              <th>Enabled/Disabled</th>
                              <th style={{width: "100px"}} >Actions</th>
                            </tr>
                          </thead>
                          <tbody style={{textAlign:"center"}}>
                            {
                                this.props.accounts.map(function (account){
                                    return (
                                        <AccountsTableRow key={account.id} account={account} />
                                    )
                                })
                            }
                          </tbody>
                        </table>
                        </div>
                      </div>
                    ) :
                    (
                        <span>Loading account details...</span>
                    )
                }
                
            </div>
            
        )
    }
}

function mapStateToProps(state){
    return {
        user: state.user,
        accounts: state.accounts
    }
}

function mapDispatchToProps(dispatch){
    return {
        loadAccounts: () => loadAccounts(dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AdminAccountActions);