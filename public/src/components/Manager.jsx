import React, { Component } from 'react';
import {connect} from 'react-redux';
import {Route, Redirect} from 'react-router-dom';

import ManagerAccountActions from './ManagerAccountActions';
import ManagerProfile from './ManagerProfile';
import ManagerFeatures from './ManagerFeatures';
import Dashboard from './Dashboard';

class Manager extends Component {
    constructor(props){
        super(props);

        this.state = {
            windowId: 0
        }
        this.changeWindow = this.changeWindow.bind(this);
    }
    changeWindow(new_window_id){
        return () => {
            var tempState = this.state;
            tempState.windowId = new_window_id;
            this.setState(tempState);
        }
        
    }
    render(){
        
        if(this.props.user){
            switch(this.props.user.access_level){
                case 0:
                    return (<Redirect to={{ pathname: "/admin", from:this.props.location }} />)
                case 2:
                    return (<Redirect to={{ pathname: "/user", from:this.props.location }} />)
                default:
                    break;
            }
        }else{
            return (
                <Redirect to={{ pathname: "/", from:this.props.location }} />
            )
        }
        var window = (null);
        switch(this.state.windowId){
            case 0:
                window = (<ManagerAccountActions />)
                break;
            case 1:
                window = (<ManagerProfile />)
                break;
            case 2:
                window = (<ManagerFeatures />)
                break;
            case 3:
                window = (<Dashboard />)
                break;
            default:
                window = (<ManagerAccountActions />)
                break;
        }
        return (
            <div>
                <div className="buttons">
                    <button className={ ["btn", (this.state.windowId == 0 ? "active" : "")].join(" ") } onClick={this.changeWindow(0)} >Accounts</button>
                    <button className={ ["btn", (this.state.windowId == 1 ? "active" : "")].join(" ") } onClick={this.changeWindow(1)} >Profile</button>
                    <button className={ ["btn", (this.state.windowId == 2 ? "active" : "")].join(" ") } onClick={this.changeWindow(2)} >Features</button>
                    <button className={ ["btn", (this.state.windowId == 3 ? "active" : "")].join(" ") } onClick={this.changeWindow(3)} >Dashboard</button>
                </div>
                
                {
                    window
                }
            </div>
            
        )
    }
}
function mapStateToProps(state){
    return {
        user: state.user
    }
}

function mapDispatchToProps(dispatch){
    return {
        loginUser: (o) => dispatch(loginUser(o))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Manager);