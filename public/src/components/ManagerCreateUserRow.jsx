import React, { Component } from 'react';
import {connect} from 'react-redux';

import {createUser} from '../actions/managerActions';

class ManagerCreateUserRow extends Component {
    constructor(props){
        super(props)
        this.handleCreate = this.handleCreate.bind(this);
    }

    handleCreate(e){
        e.preventDefault();
        var obj = {};
        if(!this.refs.email.value){
            alert("Required field: Email");
            return;
        }
        for(var key in this.refs){
            console.log(this.refs[key]);
        }

        obj.email = this.refs.email.value;
        this.refs.email.value = "";
        this.props.createUser(obj)
    }

    render(){
        return (
            <div className="createUserForm">
                <form style={{ display: "grid", gridTemplateColumns: "8fr 2fr", gridColumnGap: "20px" }} onSubmit={this.handleCreate} >
                    <input className="form-control" type="email" ref="email" placeholder="Email" />
                    <button className="btn btn-primary" type="submit" >Add New User</button>
                </form>
            </div>
        )
    }
}

function mapStateToProps(state){
    return {
        user: state.user
    }
}

function mapDispatchToProps(dispatch){
    return {
        createUser: (obj) => createUser(dispatch, obj)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ManagerCreateUserRow);