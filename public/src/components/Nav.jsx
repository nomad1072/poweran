import React, { Component } from 'react';
import {connect} from 'react-redux';
import { Link } from "react-router-dom";

class Nav extends Component {
    render(){
        if(this.props.user){
            var links = [];
            switch(this.props.user.access_level){
                case 0:
                // links.push((
                //     <Link to="/admin" >Dashboard</Link>
                // ));
                break;

                case 1:
                links.push((<a>{this.props.user.email}</a>))
                links.push((
                    <Link key={links.length} to="/manager" >Dashboard</Link>
                ));
                break;

                case 2:
                links.push((
                    <Link key={links.length} to="/user" >Dashboard</Link>
                ));
                break;

                default:
                links.push((<Link key={links.length} to="/user" >Dashboard</Link>));
                break;
            }
            links.push((<Link key={links.length} to="/logout" >Logout</Link>));
            return (
                <nav className="navbar navbar-inverse">
                    <div className="container-fluid">
                        <div className="navbar-header">
                        <a className="navbar-brand" href="#">Convergytics</a>
                        </div>
                        <ul className="nav navbar-nav">
                            {
                                links.map( (link, i) => 
                                (
                                    <li key={i} >
                                        {link}
                                    </li>
                                )
                                )
                            }
                        </ul>
                    </div>
                </nav>
                
            )
        }else{
            return (
                <nav className="navbar navbar-inverse">
                    <div className="container-fluid">
                        <div className="navbar-header">
                        <a className="navbar-brand" href="/">Convergytics</a>
                        </div>
                        <ul className="nav navbar-nav">
                        <li className="active"><Link to="/" >Home</Link></li>
                        </ul>
                    </div>
                </nav>
                
            )
        }
        
    }
}

function mapStateToProps(state){
    return {
        user: state.user
    }
}

function mapDispatchToProps(dispatch){
    return {
        
    }
}

export default connect(mapStateToProps, mapDispatchToProps) (Nav)