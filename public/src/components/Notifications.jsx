import React, { Component } from 'react';
import {connect} from 'react-redux';

class Notifications extends Component {
    render(){
        return (
            <ul className="notificationsPanel">
                {
                    this.props.notifications ?
                    this.props.notifications.map(notif => 
                        (
                            <li key={notif.id} >
                                {
                                    notif.message
                                }
                            </li>
                        )
                    ) :
                    (
                        null
                    )
                }
            </ul>
        )
    }
}

function mapStateToProps(state){
    return {
        notifications: state.notifications
    }
    
}

export default connect (mapStateToProps) (Notifications);