import React, { Component } from 'react';
import {connect} from 'react-redux';

import { addNotification } from '../actions/notificationsActions';

class Signup extends Component {
    constructor(props){
        super(props)


        this.handleSignup = this.handleSignup.bind(this);
    }
    handleSignup(e){
        e.preventDefault();
        var self = this;
        if(!this.refs.email.value){
            addNotification("Required field: Email")
            return;
        }
        if(!this.refs.fd_domain.value){
            addNotification("Required field: Freshdesk domain")
            return;
        }
        if(!this.refs.password.value){
            addNotification("Required field: Password")
            return;
        }

        fetch("http://localhost:8000/signup", {
            method: "post",
            headers: {
                "content-type": "application/json"
            },
            body: JSON.stringify({
                email: this.refs.email.value,
                fd_domain: this.refs.fd_domain.value,
                password: this.refs.password.value
            })
        })
        .then(response => response.json())
        .then(function(data){
            console.log(data);
            if(data.created){
                self.props.addNotification("Signed you up. Log In to explore!")
                
            }else{
                self.props.addNotification(data.message + ".. Try again.")
                
            }
        })
        .catch(err => self.props.addNotification(err));
    }
    render(){
        return (
            
            <div className="tab-pane fade" id="signup">
                <form onSubmit={this.handleSignup} >
                    
                    <div className="form-group">
                        <label>Email address</label>
                        <input className="form-control" type="email" name="email" ref="email" placeholder="Email" required />
                       
                    </div>
                    
                    <div className="form-group">
                        <label style={{display: "block"}} >Domain name</label>
                        <input style={{display: "inline", width: "200px" }} className="form-control" type="text" name="fd_domain" ref="fd_domain" placeholder="Domain" required />
                        <span style={{ fontSize: "17px", padding: "3px"}} >.freshdesk.com</span>
                        
                    </div>
                    <div className="form-group">
                        <label>Create password</label>
                        <input className="form-control" type="password" name="password" ref="password" placeholder="Password" required />
                    </div>  
                    <div className="form-group">
                        <button type="submit" className="btn btn-primary btn-block"> Register  </button>
                    </div>      
                </form>
            </div>
        )
    }
}

function mapStateToProps(state){
    return {
        
    }
}

function mapDispatchToProps(dispatch){
    return {
        addNotification: (message) => addNotification(dispatch, message)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Signup);