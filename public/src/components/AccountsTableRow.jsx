import React, { Component } from 'react';
import {connect} from 'react-redux';

import {editAccount, deleteAccount} from '../actions/adminActions';

class AccountsTableRow extends Component {
    constructor(props){
        super(props)
        this.handleDelete = this.handleDelete.bind(this);
    }

    handleDelete(){
        var ask = confirm(`Are you sure you want to delete user ${this.props.account.email}?`);

        if(ask){
            this.props.deleteAccount(this.props.account.id)
        }
    }

    render(){
        return (
            <tr key={this.props.account.id} >
                <td>{this.props.account.email}</td>
                <td><strong>{this.props.account.fd}</strong>.freshdesk.com</td>
                <td>{ this.props.account.enabled ? "Enabled" : "Disabled" }</td>
                <td>
                    <button className="btn btn-default" onClick={() => this.props.editAccount(this.props.account)} >{!this.props.account.enabled ? "Enable": "Disable"}</button>
                    <button className="btn btn-danger" onClick={this.handleDelete}>Delete</button>
                </td>
            </tr>
        )
    }
}

function mapStateToProps(state){
    return {
        accounts: state.accounts
    }
}

function mapDispatchToProps(dispatch){
    return {
        editAccount: (account) => editAccount(dispatch, account),
        deleteAccount: (id) => deleteAccount(dispatch, id)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AccountsTableRow);