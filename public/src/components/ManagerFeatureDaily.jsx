import React, { Component } from 'react';

import {connect} from 'react-redux';

import { loadManagerFeatures, handleFeatureUpdate } from '../actions/managerActions';

class ManagerFeatureDaily extends Component {
    constructor(props){
        super(props);

        this.handleUpdate = this.handleUpdate.bind(this);
    }

    componentWillMount(){
        this.props.loadManagerFeatures("daily");
    }

    handleUpdate(e, function_type){
        e.preventDefault();
        var obj = {
            name: "daily",
            values: {}
        };
        for(var ref in this.refs){
            obj.values[ref] = this.refs[ref].value;
        }
        
        this.props.handleFeatureUpdate(obj);

    }

    render(){
        return (
            <div>
                
                {
                    this.props.manager && this.props.manager.features && this.props.manager.features.daily ?
                    (
                        <div className="well clearfix tabhead" >
                            <form onSubmit={(e) => this.handleUpdate(e,"property")} >
                                    <div className="row">
                                        <div className="form-group">
                                            <div className="col-md-12">
                                                <strong>
                                                    <label>
                                                    Ticket Property Update URL
                                                    </label>
                                                </strong>
                                                <input className="form-control" defaultValue={this.props.manager.features.daily.property_dump_url} type="text" name="" ref="property_dump_url"/>
                                            </div>
                                        </div>
                                    </div>
                                    <br/>
                                    <div className="row">
                                        <div className="form-group">
                                            <div className="col-md-12">
                                                <strong>
                                                    <label>
                                                        Time
                                                    </label>
                                                </strong>
                                                <input className="form-control" defaultValue={this.props.manager.features.daily.property_dump_time}  type="time" ref="property_dump_time" />
                                            </div>
                                        </div>
                                    </div>
                                    <br/>
                                <button className="btn btn-primary" type="submit">Set</button>
                            </form>
                            <br/>
                            <form onSubmit={(e) => this.handleUpdate(e, "activity")} >
                                <div className="row">
                                    <div className="form-group">
                                        <div className="col-md-12">
                                            <strong>
                                                <label>
                                                Ticket Activity Update URL
                                                </label>
                                            </strong>
                                            <input className="form-control" defaultValue={this.props.manager.features.daily.activity_dump_url} type="text" name="" ref="activity_dump_url"/>
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <div className="row">
                                    <div className="form-group">
                                        <div className="col-md-12">
                                            <strong>
                                                <label>
                                                    Time
                                                </label>
                                            </strong>
                                            <input className="form-control" defaultValue={this.props.manager.features.daily.activity_dump_time} type="time" ref="activity_dump_time" />
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <button className="btn btn-primary" type="submit">Set</button>
                            </form>
                        </div>
                        
                    ) :
                    (
                        <div className="well clearfix tabhead" >
                            <form onSubmit={(e) => this.handleUpdate(e,"property")} >
                                    <div className="row">
                                        <div className="form-group">
                                            <div className="col-md-12">
                                                <strong>
                                                    <label>
                                                    Ticket Property Update URL
                                                    </label>
                                                </strong>
                                                <input className="form-control" type="text" name="" ref="property_dump_url"/>
                                            </div>
                                        </div>
                                    </div>
                                    <br/>
                                    <div className="row">
                                        <div className="form-group">
                                            <div className="col-md-12">
                                                <strong>
                                                    <label>
                                                        Time
                                                    </label>
                                                </strong>
                                                <input className="form-control" type="time" ref="property_dump_time" />
                                            </div>
                                        </div>
                                    </div>
                                    <br/>
                                <button className="btn btn-primary" type="submit">Set</button>
                            </form>
                            <br/>
                            <form onSubmit={(e) => this.handleUpdate(e, "activity")} >
                                <div className="row">
                                    <div className="form-group">
                                        <div className="col-md-12">
                                            <strong>
                                                <label>
                                                Ticket Activity Update URL
                                                </label>
                                            </strong>
                                            <input className="form-control" type="text" name="" ref="activity_dump_url"/>
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <div className="row">
                                    <div className="form-group">
                                        <div className="col-md-12">
                                            <strong>
                                                <label>
                                                    Time
                                                </label>
                                            </strong>
                                            <input className="form-control" type="time" ref="activity_dump_time" />
                                        </div>
                                    </div>
                                </div>
                                <br/>
                                <button className="btn btn-primary" type="submit">Set</button>
                            </form>
                        </div>
                    )
                }
            </div>
        )
    }
}

function mapStateToProps(state){
    return {
        user: state.user,
        manager: state.manager
    }
}

function mapDispatchToProps(dispatch){
    return {
        loadManagerFeatures: (productName) => loadManagerFeatures(dispatch, productName),
        handleFeatureUpdate: (o) => handleFeatureUpdate(dispatch, o)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ManagerFeatureDaily);