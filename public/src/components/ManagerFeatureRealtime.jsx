import React, { Component } from 'react';

import {connect} from 'react-redux';

import { loadManagerFeatures, handleFeatureUpdate } from '../actions/managerActions';

class ManagerFeatureRealtime extends Component {
    constructor(props){
        super(props);
        this.state = {
            
        }

        this.handleUpdate = this.handleUpdate.bind(this);
    }

    componentWillMount(){
        this.props.loadManagerFeatures("realtime");
    }

    handleUpdate(e){
        e.preventDefault();
        var obj = {
            name: "realtime",
            values: {}
        };
        for(var ref in this.refs){
            obj.values[ref] = this.refs[ref].checked;
        }
        this.props.handleFeatureUpdate(obj);
        console.log(obj);

    }

    render(){
        return (
            <div>
                {
                    this.props.manager && this.props.manager.features && this.props.manager.features.realtime ?
                    (
                        <div className="well clearfix tabhead">
                            <form onSubmit={this.handleUpdate} >
                                <span style={{ display: "inline-block", margin: "10px" }} >
                                    <input type="checkbox" ref="onTicketCreate" defaultChecked={this.props.manager.features.realtime.onTicketCreate || false} name=""/>
                                    <strong>Ticket Create</strong>
                                </span>
                                
                                <span style={{ display: "inline-block", margin: "10px" }} >
                                    <input type="checkbox" ref="onTicketUpdate" defaultChecked={this.props.manager.features.realtime.onTicketUpdate || false} name=""/>
                                    <strong>Ticket Update</strong>
                                </span>
                                
                                <span style={{ display: "inline-block", margin: "10px" }} >
                                    <input type="checkbox" ref="onContactCreate" defaultChecked={this.props.manager.features.realtime.onContactCreate || false} name=""/>
                                    <strong>Contact Create</strong>
                                </span>
                                
                                <span style={{ display: "inline-block", margin: "10px" }} >
                                    <input type="checkbox" ref="onContactUpdate" defaultChecked={this.props.manager.features.realtime.onContactUpdate || false} name=""/>
                                    <strong>Contact Update</strong>
                                </span>
                                
                                <span style={{ display: "inline-block", margin: "10px" }} >
                                    <input type="checkbox" ref="onConversationCreate" defaultChecked={this.props.manager.features.realtime.onConversationCreate || false} name=""/>
                                    <strong>Conversation Create</strong>
                                </span>
                                <br/>
                                <br/>
                                <button className="btn btn-primary" type="submit">Save</button>    
                            </form>
                        </div>
                        
                        
                    ) :
                    (
                        <div className="well clearfix tabhead">
                            <form onSubmit={this.handleUpdate} >
                                <span style={{ display: "inline-block", margin: "10px" }} >
                                    <input type="checkbox" ref="onTicketCreate" name=""/>
                                    <strong>Ticket Create</strong>
                                </span>
                                
                                <span style={{ display: "inline-block", margin: "10px" }} >
                                    <input type="checkbox" ref="onTicketUpdate" name=""/>
                                    <strong>Ticket Update</strong>
                                </span>
                                
                                <span style={{ display: "inline-block", margin: "10px" }} >
                                    <input type="checkbox" ref="onContactCreate" name=""/>
                                    <strong>Contact Create</strong>
                                </span>
                                
                                <span style={{ display: "inline-block", margin: "10px" }} >
                                    <input type="checkbox" ref="onContactUpdate" name=""/>
                                    <strong>Contact Update</strong>
                                </span>
                                
                                <span style={{ display: "inline-block", margin: "10px" }} >
                                    <input type="checkbox" ref="onConversationCreate" name=""/>
                                    <strong>Conversation Create</strong>
                                </span>
                                <br/>
                                <br/>
                                <button className="btn btn-primary" type="submit">Save</button>    
                            </form>
                        </div>
                    )
                }
            </div>
        )
    }
}

function mapStateToProps(state){
    return {
        user: state.user,
        manager: state.manager
    }
}

function mapDispatchToProps(dispatch){
    return {
        loadManagerFeatures: (productName) => loadManagerFeatures(dispatch, productName),
        handleFeatureUpdate: (o) => handleFeatureUpdate(dispatch, o)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ManagerFeatureRealtime);