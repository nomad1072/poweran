const DB_URL = "mongodb://localhost:27017";
const APP_NAME = "poweran";

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSchema = new Schema({
    username: String,
    password: String,
    access_level: Number
});

var User = mongoose.model('User', userSchema);

mongoose.connect(`${DB_URL}/${APP_NAME}`);
var db = mongoose.connection;

var users = [
    {
        username: "sum",
        password: "123",
        access_level: 0
    },
    {
        username: "sam",
        password: "234",
        access_level: 1
    }
]

User.collection.insert(users, function(err, docs){
    if(err){
        console.error(err);
    }else{
        console.log(docs);
        mongoose.connection.close();
    }
});