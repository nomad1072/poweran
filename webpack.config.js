const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = (env, options) => {
    const isDevMode = options.mode === "development";
    return {
    watch: false,
    entry: "./public/src/index.jsx",
    output: {
        path: path.join(__dirname, 'public/dist'),
        filename: 'bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: '/node_modules/',
                use: {
                    loader: 'babel-loader'
                }
            },
            {
                test: /\.scss$/,
                loader: 'style-loader!css-loader!sass-loader'
            },
            {
                test: /\.(jpe?g|png|gif|svg|ico)$/i,
                use: [
                    "file-loader"
                ]
            }
        ]
    },
    resolve: {
        extensions: ['.', '.js', '.jsx']
    },
    plugins: [
        new HtmlWebpackPlugin({
            filename: "index.html",
            template: "public/src/index.html"
        }),
        new webpack.DefinePlugin({
            APP_SERVER: (isDevMode ? JSON.stringify('"http://localhost:9090"') : JSON.stringify('"/"'))
        })
    ],
    devServer: {
        contentBase: path.join(__dirname, 'public/dist'),
        compress: true,
        port: 9000,
        historyApiFallback: true
    }
}
}