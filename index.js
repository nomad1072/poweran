const DB_URL = "mongodb://localhost:27017";
const APP_NAME = "poweran";
const PUBLIC_TEMPLATE = "./public/dist/index.html";

var path = require('path');
var express = require("express");
var app = express();
var http = require("http").Server(app);

var passport = require("passport");
var LocalStrategy = require("passport-local").Strategy;

var bodyParser = require("body-parser");
var multer = require("multer");
var upload = multer();
var cookieParser = require("cookie-parser");
var session = require("express-session");

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSchema = new Schema({
    email: String,
    password: String,
    fd: String,
    access_level: Number,
    enabled: Boolean,
    users: [],
    API_key: String,
    fd_key: String,
    features: Object,
    fdesk_callback: String
});

var User = mongoose.model('User', userSchema);

mongoose.connect(`${DB_URL}/${APP_NAME}`);
var db = mongoose.connection;

app.use(bodyParser.json({ type: "*/*" }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(session({
    resave: true,
    saveUninitialized: false,
    secret: "my_super_secret_key"
}));
app.use(passport.initialize());
app.use(passport.session());

app.all('/*', function(req, res, next){
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "*");
    next();
});

// check logged in status
app.all("/*", function(req, res, next){
    console.log(`${req.method} ${req.path}:`);
    if(req.session.passport && req.session.passport.user){
        console.log(req.session.passport);
    }else{
        console.log("Not logged in");
    }
    
    next();
})

app.use(express.static(path.join(__dirname, "public/dist")));
app.get("/robots.txt", (req, res) => res.sendFile(__dirname + "/robots.txt"));
app.get("/favicon.ico", (req, res) => res.sendFile(__dirname + "/public/img/favicon.ico"));

passport.use(new LocalStrategy({
    usernameField: "email",
    passwordField: "password"
}, function(email, password, done){
    console.log("here");

    User.findOne({
        email: email,
        password: password
    }, function(err, user){
        if(!err){
            if(!user){
                return done(null, false);
            }else{
                return done(null, user);
            }
        }else{
            return done(err);
        }
    })

}));

passport.serializeUser(function(user, done) {
    
    if(user){
        console.log("in serialize: ");
        console.log(user);
        console.log("returned object: ");
        console.log(user._id);
        done(null, user._id);
    }
    
});

passport.deserializeUser(function(user_id, done) {
    console.log("in deserialize: ");
    console.log(user_id);
    
    User.findOne({
        _id: user_id
    }, function(err, myUser){
        let serializedUser = {
            id: myUser.id,
            email: myUser.email,
            fd: myUser.fd,
            access_level: myUser.access_level,
            users: myUser.users,
            API_key: myUser.API_key,
            fd_key: myUser.fd_key
        };

        done(null, serializedUser);
    })

});


app.get('/', (req, res) => {
    if(req.session.passport && req.session.passport.user){
        res.redirect('/secret');
    }else{
        res.sendFile(path.join(__dirname, PUBLIC_TEMPLATE));
    }
});

app.get('/admin', (req, res) => {res.sendFile(path.join(__dirname, PUBLIC_TEMPLATE));});
app.get('/manager', (req, res) => {res.sendFile(path.join(__dirname, PUBLIC_TEMPLATE));});
app.get('/user', (req, res) => {res.sendFile(path.join(__dirname, PUBLIC_TEMPLATE));});

app.post('/register_fdesk_hook', upload.array(), function(req, res){
    console.log(req.body);
    User.findOne({"API_key": req.body.API_key}, function(err, data){
        if(data){
            data.set({fdesk_callback: req.body.callbackUrl});
            data.save(function(err){
                if(err){
                    res.json({
                        success: false,
                        message: "Could not set hooks"
                    });
                }else{
                    res.json({
                        success: true,
                        message: `Successfully set hooks!`
                    })
                }
            });
        }else{
            res.json({
                success: false,
                message: "Invalid API key"
            })
        }
        
    });
    console.log(req.body);
});

app.get('/currentUser', function(req, res){
    if(!req.user){
        res.json({
            user: null
        });
        return;
    }

    res.json({
        user: req.user
    })

    // DEV TESTING ONLY
    // {
    // User.findOne({access_level: 1}, function(err, data){
    //     res.json({
    //         user: data
    //     });
    // })
    // }
});

// FOR ADMIN ONLY ----- START
{
function checkAdminRest(req, res, next){
    if(!req.user){
        res.json({});
        return;
    }
    if(req.user.access_level != 0){
        res.json({});
        return;
    }
    console.log(req.user);
    next();
}

app.get("/accounts", checkAdminRest, function(req, res){

    User.find({access_level: 1}, function(err, result){
        var usersCleaned = result.map(function(elem){
            return {
                email: elem.email,
                fd: elem.fd,
                id: elem._id,
                enabled: elem.enabled
            }
        })
        res.json(usersCleaned);
    });
})

app.post("/account/edit", checkAdminRest, upload.array(), function(req, res){
    User.findById(req.body.id, function(err, user){
        if(err) return console.error("Error: ", err);
        if(user){
            user.set({enabled: req.body.enabled});
            user.save(function (err, updatedUser){
                res.json({
                    id: updatedUser._id,
                    fd: updatedUser.fd,
                    email: updatedUser.email,
                    enabled: updatedUser.enabled
                });
            })
        }else{
            res.json({
                _id: null
            })
        }

        
    })
})

app.post("/account/delete", checkAdminRest, upload.array(), function(req, res){
    User.deleteOne({_id: req.body.id}, function(err){
        if(err) return console.error("Error: ", err);

        res.json({
            accountDeleted: true
        });

    })
})

app.post("/admin/changepassword", checkAdminRest, function(req, res){
    var myUser = req.user;
    // var myUser = {"_id":"5b8cfa07021e29365c3532f0","email":"a","password":"e","access_level":0};
    User.findById(myUser.id, function(err, data){
        if(data.password != req.body.pass_old){
            res.json({
                success: false,
                message: "Incorrect password."
            })
        }else{
            data.set({password: req.body.pass_new});
            data.save(function(err){
                if(err){
                    res.json({
                        success: true
                    })
                }else{
                    res.json({
                        success: true
                    })
                }
            })
        }
    })
})
}
// FOR ADMIN ONLY ----- END

// FOR MANAGER ONLY ----- START
{
function checkManagerRest(req, res, next){
    if(!req.user){
        res.json({});
        return;
    }
    if(req.user.access_level != 1){
        res.json({});
        return;
    }
    console.log(req.user);
    next();
}
app.get("/manager/getchildren", checkManagerRest, function(req, res){
    // var myUser = req.user; // USE THIS WHEN MANAGER IS LOGGED IN

    User.findById(req.user.id, function(err, manager){
        console.log(manager.users);
        User.find({
            '_id': {
                $in: manager.users
            }
        }, function(err, children){
            if(err){
                res.json({
                    success: false,
                    message: "Some error occured"
                })
            }else{
                res.json({
                    success: true,
                    children: children.map(function(child) { return { _id:child._id, email:child.email, enabled:child.enabled }})
                })
            }
        })
    })
})

app.post("/user/editStatus", checkManagerRest, upload.array(), function(req, res){
    if(req.user.users.find(elem => elem == req.body.id)){
        User.findById(req.body.id, function(err, user){
            if(err){
                res.json({
                    success: false
                })
            }else{
                user.set({enabled: req.body.enabled});
                user.save(function(err, updatedUser){
                    console.log(updatedUser);
                    res.json({
                        success: true,
                        user: updatedUser
                    })
                })
            }
        })
    }else{
        res.json({
            success: false
        })
    }
    
});

app.post("/user/delete", upload.array(), function(req, res){
    var myUser = req.user; // USE THIS WHEN MANAGER IS LOGGED IN
    // var myUser = {"id":"5b8e12c991d8c71360c6615b","email":"b","password":"b","fd":"a","access_level":1,"enabled":false,"users":["5b8e132e91d8c71360c6615c","5b8f5d08e5d43d13609386f5"]};
    if(req.user.users.find(elem => elem == req.body.id)){
        User.deleteOne({_id: req.body.id}, function(err){
            if(err){
                res.json({
                    success: false,
                    message: "Error"+err
                })
            }else{
                User.findById(myUser.id, function(err, data){
                    var usersArray = data.users;
                    var myIndex = usersArray.findIndex(elem => elem == req.body.id );
                    usersArray.splice(myIndex, 1);
                    data.set({users: usersArray});
                    data.save(function(err){
                        if(err){
                            console.error(`Could not detach user ${req.body.id} from ${myUser.email}`);
                        }
                        res.json({
                            success: true
                        })
                    })
                })
                
            }
        })
    }else{
        res.json({
            success: false
        })
    }
    
})

app.post("/user/create", checkManagerRest, upload.array(), function(req, res){
    var myUser = req.user; // USE THIS WHEN MANAGER IS LOGGED IN
    // var myUser = {"_id":"5b8e12c991d8c71360c6615b","email":"b","password":"b","fd":"a","access_level":1,"enabled":false,"users":["5b8e132e91d8c71360c6615c","5b8f5d08e5d43d13609386f5"]};
    var dict = "1234567890QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm";
    var dict_length = dict.length;
    var KEY_LENGTH = 8;
    var keygen = "";
    for(var i=0; i < KEY_LENGTH; i++ ){
        keygen += dict[Math.floor(Math.random()*dict_length)];
    }
    User.collection.insert({
        email: req.body.email,
        password: keygen,
        fd: myUser.fd,
        API_key: myUser.API_key,
        fd_key: myUser.fd_key,
        access_level: 2,
        enabled: true
    }, function(err, response){
        if(err){
            console.error(`Error creating user for ${myUser.email}: `, err);
            res.json({
                success: false,
                error: "Error"+err
            })
        }else{
            User.findById(myUser.id, function(err, data){
                if(err){
                    console.error(err);
                    return;
                }
                var usersArray = data.users;
                usersArray.push(response.ops[0]._id);
                data.set({users: usersArray });
                data.save(function(err){
                    if(err){
                        console.error(`Could not add user for ${myUser.email}`);
                    }
                })
            })
            console.log(`Created user for ${myUser.email}: `, response);
            res.json({
                success: true,
                user: response.ops[0]
            })
        }
    })
})

app.post("/manager/changepassword", function(req, res){
    var myUser = req.user;
    // var myUser = {"_id":"5b8e12c991d8c71360c6615b","email":"b","password":"b","fd":"a","access_level":1,"enabled":false,"users":["5b8f69cac2fc043818d6b766","5b8f6e99179f1610242e354f","5b8f6ebaf7e6d3304ce9dd76"],"__v":23};
    User.findById(myUser.id, function(err, data){
        if(data.password != req.body.pass_old){
            res.json({
                success: false
            })
        }else{
            data.set({password: req.body.pass_new});
            data.save(function(err){
                if(err){
                    res.json({
                        success: true
                    })
                }else{
                    res.json({
                        success: true
                    })
                }
            })
        }
    })
})

app.post("/manager/updateprofile", function(req, res){
    var myUser = req.user;
    // var myUser = {"_id":"5b8e12c991d8c71360c6615b","email":"b","password":"b","fd":"a","access_level":1,"enabled":false,"users":["5b8f69cac2fc043818d6b766","5b8f6e99179f1610242e354f","5b8f6ebaf7e6d3304ce9dd76"],"__v":23};
    User.findById(myUser.id, function(err, data){
        var oo = {};
        console.log(req.body);
        for(var key in req.body){
            oo[key] = req.body[key];
        }
        User.updateMany({_id: { $in: myUser.users }}, oo, function(err){
            if(err){
                console.error(err);
                return;
            }
        })
        data.set(oo);
        data.save(function(err){
            if(err){
                res.json({
                    success: true
                })
            }else{
                res.json({
                    success: true
                })
            }
        })
        
    })
})

app.get("/manager/features/load/:product", function(req, res){
    var myUser = req.user;
    // var myUser = {"_id":"5b8e12c991d8c71360c6615b","email":"b","password":"b","fd":"a","access_level":1,"enabled":false,"users":["5b8f69cac2fc043818d6b766","5b8f6e99179f1610242e354f","5b8f6ebaf7e6d3304ce9dd76"],"__v":23};
    User.findById(myUser.id, function(err, data){
        if(err){
            res.json({
                success: false
            })
        }else{
            var response = {
                success: true
            };
            response.features = {};
            response.features[req.params.product] =  data.features[req.params.product]
            res.json(response);
        }
    })
})

app.post("/manager/features/update/:product", upload.array(), function(req, res){
    var myUser = req.user;
    // var myUser = {"_id":"5b8e12c991d8c71360c6615b","email":"b","password":"b","fd":"a","access_level":1,"enabled":false,"users":["5b8f69cac2fc043818d6b766","5b8f6e99179f1610242e354f","5b8f6ebaf7e6d3304ce9dd76"],"__v":23};
    User.findById(myUser.id, function(err, data){
        if(err){
            res.json({
                success: false,
                error: err
            })
            return;
        }
        if(data.features){
            var featuresBase = {
                ...data.features
            }
        }else{
            var featuresBase = {};
        }
        switch(req.params.product){
            case "realtime":
            data.set({features: {...featuresBase, realtime: req.body}});
            break;
    
            case "daily":
            data.set({features: {...featuresBase, daily: req.body}});
            break;
    
            case "period":
            data.set({features: {...featuresBase, period: req.body}});
            break;
    
            default:
            res.json({
                success: false,
                error: "Could not find feature type"
            })
            return;
        }
        data.save(function(err){
            if(err){
                res.json({
                    success: false,
                    error: err
                })
            }else{
                res.json({
                    success: true,
                    message: `Updated feature: ${req.params.product}`
                })
            }
        })

    })
    
})

}
// FOR MANAGER ONLY ----- END

app.post("/signup", upload.array(), function(req, res){
    console.log(req.body);
    User.find({email: req.body.email}, function(err, result){
        if(!err){
            if(result.length){
                res.json({
                    created: false,
                    message: "User already exists"
                })
            }else{
                var dict = "1234567890QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm";
                var dict_length = dict.length;
                var KEY_LENGTH = 16;
                var keygen = "";
                for(var i=0; i < KEY_LENGTH; i++ ){
                    keygen += dict[Math.floor(Math.random()*dict_length)];
                }
                User.collection.insert({
                    email: req.body.email,
                    password: req.body.password,
                    fd: req.body.fd_domain,
                    access_level: 1,
                    enabled: true,
                    fd_key: null,
                    API_key: keygen,
                    features: {}

                }, function(err, response){
                    if(err){
                        console.error(err);
                        res.json({
                            created: false,
                            message: "Some error occured."
                        })
                    }else{
                        console.log(response);
                        res.json({
                            created: true,
                            message: "Successfully signed you up"
                        })
                    }

                })
            }
        }else{

        }
    });
})

app.post("/login", function(req, res, next){
    passport.authenticate('local', function(err, user, info){
        if(err) { return next(err)}
        if(!user){ res.json({ loggedIn: false, message: "Invalid username or password" }) }
        req.logIn(user, function(err){
            if(err) { return next(err)}
            return res.json({ loggedIn: true, user: req.user })
        })
    }) (req, res, next);
});

app.get("/logout", (req, res) => {
    
    req.logout();
    res.json({
        user: null
    });
});

app.all('/*', (req, res) => res.json({
    "message": "Not found",
    "status": "404"
}));

http.listen(8000,()=> {
    console.log("listening on *:8000");
});